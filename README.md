# submodule-comment

My thoughts on submodules

The "parent" of (the project containing) a submodule effectively _pins_ it by referencing the submodule by remote and SHA.
Either of these can be detached from reality, making it effectively impossible to clone or use the parent.

Problem: The recorded SHA might not be in the public repo of the submodule.
This can happen if a commit was made in the submodule, that new submodule version was then added to the parent, and the parent pushed, without pushing the submodule.
A variant on this is where that commit is a merge commit, as suggested by the workflow in https://git-scm.com/book/en/v2/Git-Tools-Submodules.
A further variant on this could be where the correct SHA did exist on the repo of the submodule, but the public version of that repo has subsequently been modified to remove that commit (normally this would be via a force push, but could conceivably be via a garbage collection if the commit was not reachable).

Problem: The remote might not be available. It could be that it is simply not a public repo. Or it could be that it uses the wrong protocol (for example, `git@github.com:blah/blah` in a situation where you don't have private SSH keys to access github (running on a Continuous Integration server)).